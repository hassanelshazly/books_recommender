require 'sidekiq/api'

sidekiq_config = Rails.application.config_for('sidekiq').with_indifferent_access
connection_string = if Rails.env.production?
                      "rediss://#{sidekiq_config[:username]}:#{sidekiq_config[:password]}@#{sidekiq_config[:host]}:#{sidekiq_config[:port]}"
                    else
                      "redis://#{sidekiq_config[:host]}:#{sidekiq_config[:port]}/#{sidekiq_config[:db]}"
                    end

Sidekiq.configure_server do |config|
  config.redis = { url: connection_string }
end

Sidekiq.configure_client do |config|
  config.redis = { url: connection_string }
end
