redis_config = Rails.application.config_for('redis').with_indifferent_access

if Rails.env.production?
  REDIS = Redis.new(
    url: "rediss://#{redis_config[:username]}:#{redis_config[:password]}@#{redis_config[:host]}:#{redis_config[:port]}"
  )
else
  REDIS = Redis.new(
    host: redis_config[:host], port: redis_config[:port], db: redis_config[:db]
  )
end
