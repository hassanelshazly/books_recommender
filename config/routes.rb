Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root 'healthchecks#index'

  namespace :api do
    namespace :v1 do
      resources :books, only: %i[index create] do
        collection do
          get :recommended
        end
      end

      resources :users, only: %i[create] do
        collection do
          post :submit_book
        end
      end
    end
  end
end
