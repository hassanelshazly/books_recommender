require 'uri'
require 'net/http'

class SmsService
  SMS_PROVIDER = ENV.fetch('SMS_PROVIDER', nil)

  def initialize(user_id)
    @user = User.find(user_id)
  end

  def send_thanks_sms
    message = ERB.new(
      File.read("#{Rails.root}/app/views/sms/thanks.text.erb")
    ).result(instance_eval { binding })

    Net::HTTP.post(
      URI(SMS_PROVIDER), { message: message }.to_json, { 'Content-Type': 'application/json' }
    )
  end
end
