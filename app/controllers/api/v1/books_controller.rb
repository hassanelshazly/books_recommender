class Api::V1::BooksController < ApplicationController
  def index
    render json: Book.all.decorate.to_json
  end

  def recommended
    render json: Book.limit(5).decorate.to_recommended_json
  end

  def create
    book = Book.create(
      name: params.require(:name),
      total_pages: params.require(:total_pages)
    )

    render json: book.decorate.to_json
  end
end
