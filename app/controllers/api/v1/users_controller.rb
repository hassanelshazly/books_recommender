class Api::V1::UsersController < ApplicationController
  before_action :prepare_user, only: %i[submit_book]
  before_action :prepare_book, only: %i[submit_book]
  before_action :validate_book_params, only: %i[submit_book]

  def submit_book
    lock_key = CacheKeys::Book.pages(@book.id)
    RedisMutex.with_lock(lock_key, block: 10, sleep: 0.1, expire: 30) do
      @book.reload # to ensure that we have the lastest record

      pages = @book.pages
      start_page = @start_page - 1
      end_page = @end_page - 1 # Assuming that the endpage is not read

      first_part = start_page.zero? ? '' : pages[..start_page - 1]
      pages = "#{first_part}#{'1' * (end_page - start_page)}#{pages[end_page..]}"
      read_pages = pages.chars.count { |x| x == '1' }

      @book.update(pages: pages, read_pages: read_pages)

      SmsSender.perform_async(@user.id)

      render json: @book.decorate.to_json, status: 200
    end
  end

  private

  def prepare_book
    book_id = params.require(:book_id)
    @book = Book.find(book_id)
  end

  def prepare_user
    user_id = params.require(:user_id)
    @user = User.find(user_id)
  end

  def validate_book_params
    @start_page = params.require(:start_page)
    @end_page = params.require(:end_page)
    return if @start_page.positive? && @start_page < @end_page && @end_page < @book.total_pages

    render json: { message: 'Invalid interval' }, status: 422
  end
end
