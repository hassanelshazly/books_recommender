class SmsSender
  include Sidekiq::Worker

  sidekiq_options queue: 'books_recommeneder:sms-sender', retry: 10

  def perform(user_id)
    SmsService.new(user_id).send_thanks_sms
  end
end
