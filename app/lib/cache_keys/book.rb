module CacheKeys
  module Book
    class << self
      def pages(book_id)
        "book:#{book_id}:pages"
      end
    end
  end
end
