class BooksDecorator < Draper::CollectionDecorator
  def to_json(*_args)
    object.map do |book|
      book.decorate.to_json
    end
  end

  def to_recommended_json
    object.map do |book|
      book.decorate.to_recommended_json
    end
  end
end
