class BookDecorator < Draper::Decorator
  delegate_all

  def to_json(*_args)
    as_json(only: %i[id name read_pages total_pages])
  end

  def to_recommended_json
    {
      book_id: id,
      book_name: name,
      num_of_read_pages: read_pages
    }
  end
end
