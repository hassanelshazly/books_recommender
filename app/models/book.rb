class Book < ApplicationRecord
  validates :name, presence: true
  validates :total_pages, presence: true

  before_create :set_pages

  default_scope -> { order(read_pages: :desc) }

  private

  def set_pages
    self.pages = '0' * total_pages
  end
end
