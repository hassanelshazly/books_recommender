FROM ruby:3.0.2 as base

RUN apt-get update -qq && apt-get install -y build-essential apt-utils libpq-dev nodejs

WORKDIR /var/app

RUN gem install bundler

COPY . .

RUN bundle install

RUN rm -rf .git
RUN rm Dockerfile

ARG DEFAULT_PORT 3000

EXPOSE ${DEFAULT_PORT}

CMD [ "./web_entrypoint.sh"]
