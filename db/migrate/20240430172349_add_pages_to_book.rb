class AddPagesToBook < ActiveRecord::Migration[7.0]
  def change
    add_column :books, :pages, :text
    add_column :books, :total_pages, :integer
  end
end
