class CreateBooks < ActiveRecord::Migration[7.0]
  def change
    create_table :books do |t|
      t.string :name
      t.bigint :read_pages

      t.timestamps
    end

    add_index :books, :read_pages
  end
end
