class AddValidationsToBook < ActiveRecord::Migration[7.0]
  def change
    change_column :books, :total_pages, :integer, default: 0, null: false
    change_column :books, :read_pages, :integer, default: 0, null: false
  end
end
