# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

Book.create(name: 'Clean Code', total_pages: 100)
Book.create(name: 'Data Intensive Applications', total_pages: 200)
Book.create(name: 'Harry Potter', total_pages: 300)

User.create(name: 'Harry')
User.create(name: 'Potter')
