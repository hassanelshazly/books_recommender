# Books Recommender

## How to run the project

1. Run the services
```
export SMS_PROVIDER=<sms_provider_api>

sudo docker compose up -d web       # To run the web service
sudo docker compose up -d sidekiq   # To run background processing

# To initialize the database
sudo docker compose exec web bin/rails db:drop db:create db:schema:load db:seed
```
2. Send requets from any web client

The docs in docs/swagger.yml

```
# To get All books
curl -X GET localhost:3000/api/v1/books

# To get Recommended books
curl -X GET localhost:3000/api/v1/books/recommended

# To submit a reading 
curl -X POST localhost:3000/api/v1/users/submit_book \
     -H 'Content-Type: application/json' \
     --data-raw '{"user_id":2,"book_id":2,"start_page":2,"end_page":20}'

# To submit a new book
curl -X POST localhost:3000/api/v1/books \
     -H 'Content-Type: application/json' \
     --data-raw '{"name":"Lord of Ranges","total_pages":600}'
```

Or you can use the deployment version on
https://books-recommender.onrender.com/

The instance will spin down with inactivity So you may face a bit of delay on the first request

Unfortunately the deployed version doesn't support SMS messageing
