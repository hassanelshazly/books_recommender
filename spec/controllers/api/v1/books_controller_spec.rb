describe Api::V1::BooksController, type: :request do
  describe '#recommended' do
    context 'when user request recommended books' do
      let!(:book1) { create :book, name: 'Test 1', read_pages: 10 }
      let!(:book2) { create :book, name: 'Test 2', read_pages: 5 }
      let!(:book3) { create :book, name: 'Test 3', read_pages: 15 }

      let(:expected_response) do
        [
          { book_id: book3.id, book_name: book3.name, num_of_read_pages: book3.read_pages },
          { book_id: book1.id, book_name: book1.name, num_of_read_pages: book1.read_pages },
          { book_id: book2.id, book_name: book2.name, num_of_read_pages: book2.read_pages }
        ]
      end

      it 'should return the recommended books' do
        get '/api/v1/books/recommended'

        expect(JSON.parse(response.body, symbolize_names: true)).to eq(expected_response)
      end
    end
  end
end
