describe Api::V1::UsersController, type: :request do
  describe '#submit_book' do
    context 'when user submit new reading' do
      let(:book) { create :book, name: 'Test', total_pages: 200 }
      let(:user) { create :user, name: 'Test' }
      let(:start_page) { 5 }
      let(:end_page) { 10 }
      let(:params) { { book_id: book.id, user_id: user.id, start_page: start_page, end_page: end_page }.to_json }
      let(:headers) { { 'Content-Type': 'application/json' } }

      before { SmsSender.clear }

      it 'should should update the page interval' do
        post '/api/v1/users/submit_book', params: params, headers: headers

        expect(book.reload.pages[(start_page - 1)..(end_page - 2)]).to eq('1' * (end_page - start_page))
      end

      it 'should update the number of read pages' do
        post '/api/v1/users/submit_book', params: params, headers: headers

        expect(book.reload.read_pages).to eq(end_page - start_page)
      end

      it 'should send sms to the user' do
        post '/api/v1/users/submit_book', params: params, headers: headers

        expect(Sidekiq::Queues[SmsSender.queue].count).to eq(1)
      end
    end
  end
end
